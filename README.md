# ![Schluss](https://www.schluss.org/media/logo-linksboven.svg) Beta App


# Table Of Contents

* [What it does](#what-it-does)
* [Requirements](#requirements)
* [Development](#development)
* [Commands](#commands)
* [Configuration](#configuration)
* [Contributing](#contributing)
* [Testing](#testing)
* [Deployment](#deployment)
* [License](#license)

## What it does

## Requirements

- Flutter (https://flutter.dev/docs/get-started/install/)

## Development

## Commands

## Configuration

## Contributing

## Testing

## Deployment

## Contributors

## License
[BSD 3-Clause](LICENSE)

